package by.rstq.syn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("consumer")
public class Controller {

    Logger logger = LoggerFactory.getLogger(Controller.class);

    private final ConfigService service;

    public Controller(ConfigService service) {
        this.service = service;
    }

    @PostMapping
    public void post(HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        logger.info("new request from "+ip);
        logic(ip);
    }

    @GetMapping
    public String get(){
        return service.getConfig().toString();
    }

    public void logic(String ip){
        Map<String,Integer> result = service.getConfig();
        if (!result.containsKey(ip)) {
            result.put(ip,1);
        } else {
            int k = result.get(ip);
            result.put(ip,k+1);
        }
    }
}
