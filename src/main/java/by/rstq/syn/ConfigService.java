package by.rstq.syn;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Service
public class ConfigService {

    private Map<String, Integer> config = new HashMap<>();

    public Map<String, Integer> getConfig() {
        return config;
    }

    public Collection<String> configIps() {
        return new ArrayList<>(config.keySet());
    }

    public void setConfig(String ip, int count) {
        config.put(ip, count);
    }

    public int getCount(String key) {
        return config.get(key);
    }

    @Override
    public String toString() {
        return "ConfigService{" +
                "config=" + config +
                '}';
    }
}
