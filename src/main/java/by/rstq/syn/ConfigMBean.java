package by.rstq.syn;

import org.springframework.jmx.export.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
@ManagedResource(description = "application config bean")
public class ConfigMBean {

    private final ConfigService configService;

    public ConfigMBean(ConfigService configService) {
        this.configService = configService;
    }

    @ManagedOperation(description = "Get ALL IP")
    public Collection<String> getConfigIp() {
        return configService.configIps();
    }

    @ManagedOperation
    @ManagedOperationParameters({
            @ManagedOperationParameter(name = "ip", description = "count of request from this ip")
    })
    public int getCount(String ip) {
        return configService.getCount(ip);
    }
}
